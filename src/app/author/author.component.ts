import {Component, ViewChild} from '@angular/core';
import {SlickCarouselComponent} from 'ngx-slick-carousel';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent{
  @ViewChild('slickModal', { static: true }) slickModal: SlickCarouselComponent;
  slides = [
    {img: '../assets/images/user.png',
    name: 'Ina Sestrjevitovschi',
    img2: '../assets/images/phone.png',
    number: '61839467',
    img3: '../assets/images/mail.png',
    email: 'test@gmail.com'},
    {img: '../assets/images/user.png',
      name: 'Ina Sestrjevitovschi',
      img2: '../assets/images/phone.png',
      number: '61839467',
      img3: '../assets/images/mail.png',
      email: 'test@gmail.com'},
    {img: '../assets/images/user.png',
      name: 'Ina Sestrjevitovschi',
      img2: '../assets/images/phone.png',
      number: '61839467',
      img3: '../assets/images/mail.png',
      email: 'test@gmail.com'},
    {img: '../assets/images/user.png',
      name: 'Ina Sestrjevitovschi',
      img2: '../assets/images/phone.png',
      number: '61839467',
      img3: '../assets/images/mail.png',
      email: 'test@gmail.com'},
    {img: '../assets/images/user.png',
      name: 'Ina Sestrjevitovschi',
      img2: '../assets/images/phone.png',
      number: '61839467',
      img3: '../assets/images/mail.png',
      email: 'test@gmail.com'},
    {img: '../assets/images/user.png',
      name: 'Ina Sestrjevitovschi',
      img2: '../assets/images/phone.png',
      number: '61839467',
      img3: '../assets/images/mail.png',
      email: 'test@gmail.com'}
  ];
  slideConfig1 = {
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 1000,
    speed: 1500,
    responsive: [
      {
        breakpoint: 992,
        slidesToShow: 3
      }
    ]
  };

  slideConfig2 = {
    slidesToShow: 5,
    slidesToScroll: 5,
    autoplaySpeed: 1000,
    speed: 500,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          autoplay: true
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          autoplay: true
        }
      }
    ]
  };
  next() {
    this.slickModal.slickNext();
  }
  prev() {
    this.slickModal.slickPrev();
  }
  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    console.log('afterChange');
  }

  beforeChange(e) {
    console.log('beforeChange');
  }
}
