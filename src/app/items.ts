export const products = [
  {
    title: 'test-1',
    img: 'https://images.pexels.com/photos/1996333/pexels-photo-1996333.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    more: 'mai mult',
    text: 'Calul este considerat animalul nobililor. Este folosit de cand lumea pentru transport, divertisment si sport. De asemenea, este unul dintre cele mai prezente animale in diverse mitologii si religii.',
    price: '200 lei',
    less: 'mai putin'
  },
{
  title: 'test-2',
    img: 'https://images.pexels.com/photos/1996333/pexels-photo-1996333.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    more: 'mai mult',
    text: 'Caii pot sa doarma in picioare si experimenteaza miscari rapide ale ochilor in timpul somnului, ceea ce inseamna ca cel mai probabil viseaza.',
    price: '300 lei',
    less: 'mai putin'
},
{
  title: 'test-3',
    img: 'https://images.pexels.com/photos/1996333/pexels-photo-1996333.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    more: 'mai mult',
    text: 'Caii beau foarte multa apa. Intr-o zi singura zi, acestia ajung sa bea peste 90 de litri de apa. Apa reprezinta 50% din greutatea totala a cailor.',
    price: '400 lei',
  less: 'mai putin'
},
{
  title: 'test-4',
    img: 'https://images.pexels.com/photos/1996333/pexels-photo-1996333.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    more: 'mai mult',
    text: 'Cei mai rezistenti cai si buni alergatori sunt considerati caii arabi. Acestia pot sa alerge fara oprire 160 de kilometri. Pana si structura scheletica a cailor arabi difera de a celorlalti.',
    price: '400 lei',
  less: 'mai putin'
},
{
    title: 'test-5',
    img: 'https://images.pexels.com/photos/1996333/pexels-photo-1996333.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    more: 'mai mult',
    text: 'In unele tari, carnea de cal este considerata o delicatesa. In Franta, de exemplu, in meniurile unor restaurante gasim carne, creier si inclusiv inima de cal, pregatite in diverse feluri.',
    price: '400 lei',
  less: 'mai putin'
}
];
