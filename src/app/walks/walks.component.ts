import { Component, ViewChild } from '@angular/core';
import { SlickCarouselComponent } from "ngx-slick-carousel";

@Component({
  selector: 'app-walks',
  templateUrl: './walks.component.html',
  styleUrls: ['./walks.component.css']
})
export class WalksComponent {
  @ViewChild('slickModal', { static: true }) slickModal: SlickCarouselComponent;
  slides = [
    {img: '../assets/images/horses1.jpg'},
    {img: '../assets/images/horses2.jpg'},
    {img: '../assets/images/horses3.jpg'},
    {img: '../assets/images/horses4.jpg'},
    {img: '../assets/images/horses5.jpg'},
    {img: '../assets/images/horses6.jpg'},
    {img: '../assets/images/horses7.jpg'},
    {img: '../assets/images/horses8.jpg'},
    {img: '../assets/images/horses9.jpg'},
    {img: '../assets/images/horses10.jpg'},
    {img: '../assets/images/horses11.jpg'},
    {img: '../assets/images/horses12.jpg'},
  ];
  slideConfig = {
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false,
    autoplay: false,
    autoplaySpeed: 1000,
    speed: 500,
    responsive : [
      {
      breakpoint: 991,
  settings: {
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true
      }
},
{
  breakpoint: 767,
    settings: {
  slidesToShow: 2,
    slidesToScroll: 2,
      autoplay: true
}
},
{
  breakpoint: 600,
    settings: {
  slidesToShow: 1,
    slidesToScroll: 1,
      autoplay: true
}
}
    ]
  };
  next() {
    this.slickModal.slickNext();
  }
  prev() {
    this.slickModal.slickPrev();
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    console.log('afterChange');
  }

  beforeChange(e) {
    console.log('beforeChange');
  }
}

