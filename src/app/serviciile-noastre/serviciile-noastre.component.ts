import { Component } from '@angular/core';
import { products } from '../items';

@Component({
  selector: 'app-serviciile-noastre',
  templateUrl: './serviciile-noastre.component.html',
  styleUrls: ['./serviciile-noastre.component.css']
})
export class ServiciileNoastreComponent{
  items = products;
  show = false;
  lgt0 = 0;
  lgt = 1;
}
